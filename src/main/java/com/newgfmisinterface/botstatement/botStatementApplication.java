package com.newgfmisinterface.botstatement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.newgfmisinterface.botstatement.properties.FileStorageProperties;


@SpringBootApplication
@EnableConfigurationProperties({
    FileStorageProperties.class
})
public class botStatementApplication {

	public static void main(String[] args) {
		SpringApplication.run(botStatementApplication.class, args);
	}

}
