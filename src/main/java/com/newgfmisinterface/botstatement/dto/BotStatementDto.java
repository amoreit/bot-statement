package com.newgfmisinterface.botstatement.dto;

public class BotStatementDto {
    private String acctnum;
    private String transdate;
    private String printcode;
    private String tranflag;
    private String narrative;
    private String tranamt;

    public String getAcctnum() {
        return acctnum;
    }

    public void setAcctnum(String acctnum) {
        this.acctnum = acctnum;
    }

    public String getTransdate() {
        return transdate;
    }

    public void setTransdate(String transdate) {
        this.transdate = transdate;
    }

    public String getPrintcode() {
        return printcode;
    }

    public void setPrintcode(String printcode) {
        this.printcode = printcode;
    }

    public String getTranflag() {
        return tranflag;
    }

    public void setTranflag(String tranflag) {
        this.tranflag = tranflag;
    }

    public String getNarrative() {
        return narrative;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }

    public String getTranamt() {
        return tranamt;
    }

    public void setTranamt(String tranamt) {
        this.tranamt = tranamt;
    }

    @Override
    public String toString() {
        return "BotStatementDto{" +
                "acctnum='" + acctnum + '\'' +
                ", transdate='" + transdate + '\'' +
                ", printcode='" + printcode + '\'' +
                ", tranflag='" + tranflag + '\'' +
                ", narrative='" + narrative + '\'' +
                ", tranamt='" + tranamt + '\'' +
                '}';
    }
}
