package com.newgfmisinterface.botstatement.dto;

public class BotStatementSaveDto {
    private String filename;
    private String transdate;
    private String acctnum;
    private String printcode;
    private String tranflag;
    private String narrative;
    private String tranamt;
    private String createddate;
    private String createdpage;
    private String createduser;
    private String ipaddr;
    private String updateddate;
    private String updatedpage;
    private String updateduser;
    private String isactive;
    private String recordstatus;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getTransdate() {
        return transdate;
    }

    public void setTransdate(String transdate) {
        this.transdate = transdate;
    }

    public String getAcctnum() {
        return acctnum;
    }

    public void setAcctnum(String acctnum) {
        this.acctnum = acctnum;
    }

    public String getPrintcode() {
        return printcode;
    }

    public void setPrintcode(String printcode) {
        this.printcode = printcode;
    }

    public String getTranflag() {
        return tranflag;
    }

    public void setTranflag(String tranflag) {
        this.tranflag = tranflag;
    }

    public String getNarrative() {
        return narrative;
    }

    public void setNarrative(String narrative) {
        this.narrative = narrative;
    }

    public String getTranamt() {
        return tranamt;
    }

    public void setTranamt(String tranamt) {
        this.tranamt = tranamt;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getCreatedpage() {
        return createdpage;
    }

    public void setCreatedpage(String createdpage) {
        this.createdpage = createdpage;
    }

    public String getCreateduser() {
        return createduser;
    }

    public void setCreateduser(String createduser) {
        this.createduser = createduser;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getUpdateddate() {
        return updateddate;
    }

    public void setUpdateddate(String updateddate) {
        this.updateddate = updateddate;
    }

    public String getUpdatedpage() {
        return updatedpage;
    }

    public void setUpdatedpage(String updatedpage) {
        this.updatedpage = updatedpage;
    }

    public String getUpdateduser() {
        return updateduser;
    }

    public void setUpdateduser(String updateduser) {
        this.updateduser = updateduser;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getRecordstatus() {
        return recordstatus;
    }

    public void setRecordstatus(String recordstatus) {
        this.recordstatus = recordstatus;
    }

    @Override
    public String toString() {
        return "BotStatementSaveDto{" +
                "filename='" + filename + '\'' +
                ", transdate='" + transdate + '\'' +
                ", acctnum='" + acctnum + '\'' +
                ", printcode='" + printcode + '\'' +
                ", tranflag='" + tranflag + '\'' +
                ", narrative='" + narrative + '\'' +
                ", tranamt='" + tranamt + '\'' +
                ", createddate='" + createddate + '\'' +
                ", createdpage='" + createdpage + '\'' +
                ", createduser='" + createduser + '\'' +
                ", ipaddr='" + ipaddr + '\'' +
                ", updateddate='" + updateddate + '\'' +
                ", updatedpage='" + updatedpage + '\'' +
                ", updateduser='" + updateduser + '\'' +
                ", isactive='" + isactive + '\'' +
                ", recordstatus='" + recordstatus + '\'' +
                '}';
    }
}
