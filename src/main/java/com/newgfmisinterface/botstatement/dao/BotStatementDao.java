package com.newgfmisinterface.botstatement.dao;

import com.newgfmisinterface.botstatement.dto.BotStatementSaveDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class BotStatementDao {
    private static Logger LOGGER = LoggerFactory.getLogger(BotStatementDao.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void add(BotStatementSaveDto DTO){
        String formatDate = new SimpleDateFormat("dd-MMM-yy HH:mm:ss").format(new Date());
        String sqlCount= "SELECT COUNT(*) AS COUNT1 FROM th_rp_trn_bot_stmt WHERE file_name='"+DTO.getFilename()+"' AND trans_date= '"+DTO.getTransdate()+"' AND print_code='"+DTO.getPrintcode()+"' AND tran_flag= '"+DTO.getTranflag()+"' AND narrative='"+DTO.getNarrative()+"' AND tran_amt='"+DTO.getTranamt()+"' ";
        int codeParams = jdbcTemplate.queryForObject(sqlCount, Integer.class);
        int lineNo = codeParams+1;
        String sql ="INSERT INTO th_rp_trn_bot_stmt (file_name,trans_date,acct_num,print_code,tran_flag,narrative,tran_amt,record_status,line_no,created_date,updated_date)" +
                "values ('"+DTO.getFilename()+"', '"+DTO.getTransdate()+"', '"+DTO.getAcctnum()+"', '"+DTO.getPrintcode()+"', '"+DTO.getTranflag()+"','"+DTO.getNarrative()+"', "+DTO.getTranamt()+",'A',"+lineNo+",TO_DATE('"+formatDate+"','DD-MON-YY HH24:MI:SS'),TO_DATE('"+formatDate+"','DD-MON-YY HH24:MI:SS'))";
        System.out.println("<<< BotStatementSaveDto: " + sql);
        jdbcTemplate.update(sql);
    }
    public List<Map<String, Object>> getPathInput() throws Exception {
        List<Map<String, Object>> resultList = null;
        String sql ="SELECT VALUE AS uploadpath FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='INPUT_PATH_BOTSTATEMENT_BATCHJOB'";
        System.out.println("<<< getPathInput: " + sql);
        resultList = jdbcTemplate.queryForList(sql);
        return resultList;
    }
}
