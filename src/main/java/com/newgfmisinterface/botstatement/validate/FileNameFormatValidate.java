package com.newgfmisinterface.botstatement.validate;

public class FileNameFormatValidate {

    public boolean isFileProjectCode(String fileName , String projectName) {

        String expFileName = fileName.toLowerCase();
        String expProjectName = projectName.toLowerCase();
        String subsTringFileName = expFileName.substring(0, 5);

        if (!subsTringFileName.equals(expProjectName)) {
            return false;
        }
        return true;
    }
    public boolean isFileProjectToProcess(String fileName , String projectName) {

        String expFileName = fileName.toLowerCase();
        String expProjectName = projectName.toLowerCase();

        if (!expFileName.equals(expProjectName)) {
            return false;
        }
        return true;
    }
}
