package com.newgfmisinterface.botstatement.validate;

import com.newgfmisinterface.botstatement.dto.BotStatementDto;

public class TextBotStatementValidate {
    public TextBotStatementValidate() {
    }

    public BotStatementDto substringLineObj(String textLine) {


        BotStatementDto botStatement = new BotStatementDto();

        String acctNum = textLine.substring(0, 10);
        botStatement.setAcctnum(acctNum);

        String transDate = textLine.substring(10, 18);
        botStatement.setTransdate(transDate);

        String printCode = textLine.substring(18, 21);
        botStatement.setPrintcode(printCode);

        String tranFlag = textLine.substring(21, 22);
        botStatement.setTranflag(tranFlag);

        String narrative = textLine.substring(22, 30);
        String trimNarrative = narrative.trim();
        botStatement.setNarrative(trimNarrative);

        String tranAmt = textLine.substring(30, 48);
        String trimTranAmt = tranAmt.trim();
        botStatement.setTranamt(trimTranAmt);


        return botStatement;

    }

}
