package com.newgfmisinterface.botstatement.job;

import com.newgfmisinterface.botstatement.dao.BotStatementDao;
import com.newgfmisinterface.botstatement.dto.BotStatementDto;
import com.newgfmisinterface.botstatement.dto.BotStatementSaveDto;
import com.newgfmisinterface.botstatement.service.FileStorageService;
import com.newgfmisinterface.botstatement.service.ScanFilePathService;
import com.newgfmisinterface.botstatement.validate.TextBotStatementValidate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

@Component
public class ProcessJobBSTMT {
    private static Logger LOGGER = LoggerFactory.getLogger(ProcessJobBSTMT.class);
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private BotStatementDao botStatementDao;

    @Autowired
    private ScanFilePathService scanFilePath;

    private TextBotStatementValidate textBotStatementValidate = new TextBotStatementValidate();

    public int jobRunner(String fileName) {
        int count = 0;
        File file = new File(fileStorageService.getFileStorageLocation().toString() + "/" + fileName);
        String filenameFullPath = file.getAbsolutePath();
        List<String> fileByline = fileStorageService.readFileByLine(filenameFullPath);
        for (String line : fileByline) {
            count = count+1;
            BotStatementDto lineObj = textBotStatementValidate.substringLineObj(line);
            BotStatementSaveDto saveBot = new BotStatementSaveDto();
            saveBot.setFilename(fileName);
            saveBot.setAcctnum(lineObj.getAcctnum());
            saveBot.setTransdate(lineObj.getTransdate());
            saveBot.setPrintcode(lineObj.getPrintcode());
            saveBot.setTranflag(lineObj.getTranflag());
            saveBot.setNarrative(lineObj.getNarrative());
            saveBot.setTranamt(lineObj.getTranamt());
System.out.println("line"+line);
            try {
                botStatementDao.add(saveBot);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        try {
            fileStorageService.MoveFiletoSuccessDirDate(fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return count;
    }
}
