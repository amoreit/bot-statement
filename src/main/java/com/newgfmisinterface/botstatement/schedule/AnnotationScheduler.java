package com.newgfmisinterface.botstatement.schedule;

import com.newgfmisinterface.botstatement.dao.SystemConfigDao;
import com.newgfmisinterface.botstatement.job.ProcessJobBSTMT;
import com.newgfmisinterface.botstatement.service.FileStorageService;
import com.newgfmisinterface.botstatement.service.ScanFilePathService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@EnableScheduling
@Service
public class AnnotationScheduler {
    private static Logger LOGGER = LoggerFactory.getLogger(AnnotationScheduler.class);
    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private ScanFilePathService scanFilePath;
    @Autowired
    private ProcessJobBSTMT processJobBSTMT;
    @Autowired
    private TaskScheduler taskScheduler;
    @Autowired
    private SystemConfigDao systemConfigDao;

    @Bean
    public String getConfigSchedule() {
        try {
            return this.systemConfigDao.getSchedulePeriodD();
        } catch (Exception e) {
            e.printStackTrace();
            return "0 0 20 * * ?";
        }
    }
    //Schedule run on period D
    @Scheduled(cron = "#{@getConfigSchedule}" , zone = "GMT+7:00")
    public void runCron() {
        long startTime = System.currentTimeMillis();

        String projectCode = "BSTMT";
        List<String> projectName = new ArrayList<>();

        List<String> moveFile = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectCode);

        int fileListForScan = 0;
        for (String moveFileToProcess : moveFile) {
            File fileName = new File(moveFileToProcess);
            String getFileName = fileName.getName();
            projectName.add(getFileName);
            fileStorageService.moveFileToProcessDir(moveFileToProcess);
            fileListForScan++;
        }
        if (fileListForScan > 0) {
            List<String> fileList = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);
            for (String filename : fileList) {
                processJobBSTMT.jobRunner(filename);
            }
        }

        long endTime = System.currentTimeMillis();
        long duration = (endTime - startTime);
    }
}
