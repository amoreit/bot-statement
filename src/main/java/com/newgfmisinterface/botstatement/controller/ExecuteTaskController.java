package com.newgfmisinterface.botstatement.controller;

import com.newgfmisinterface.botstatement.job.ProcessJobBSTMT;
import com.newgfmisinterface.botstatement.service.FileStorageService;
import com.newgfmisinterface.botstatement.service.ScanFilePathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/executebot")
public class ExecuteTaskController {
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ScanFilePathService scanFilePath;

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private ProcessJobBSTMT processJobBSTMT;

    @GetMapping
    public ResponseEntity<String> executeTask() {
        long startTime = System.currentTimeMillis();

        String projectCode = "BSTMT";
        List<String> projectName = new ArrayList<>();

        List<String> moveFile = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectCode);

        int fileListForScan = 0;
        for (String moveFileToProcess : moveFile) {
            File fileName = new File(moveFileToProcess);
            String getFileName = fileName.getName();
            projectName.add(getFileName);
            fileStorageService.moveFileToProcessDir(moveFileToProcess);
            fileListForScan++;
        }
        if (fileListForScan > 0) {
            List<String> fileList = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);
            for (String filename : fileList) {
//                processJobBSTMT.jobRunner(filename);
            }
        }

        long endTime = System.currentTimeMillis();
        long duration = (endTime - startTime);
        System.out.print("Execution time is " + duration + " milli seconds\n");

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        return new ResponseEntity<String>("<h1>Job Task bot-statement is Running at Time : " + timeStamp + "</h1>", HttpStatus.OK);
    }
}
