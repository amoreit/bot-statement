package com.newgfmisinterface.botstatement.controller;

import com.newgfmisinterface.botstatement.dto.BotStatementSaveDto;
import com.newgfmisinterface.botstatement.dto.ResponseDto;
import com.newgfmisinterface.botstatement.dao.BotStatementDao;
import com.newgfmisinterface.botstatement.job.ProcessJobBSTMT;
import com.newgfmisinterface.botstatement.service.FileStorageService;
import com.newgfmisinterface.botstatement.service.ScanFilePathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class BotstatementController {
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ScanFilePathService scanFilePath;

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private BotStatementDao botStatementDao;

    @Autowired
    private ProcessJobBSTMT processJobBSTMT;

    @GetMapping("/pathInput")
    public ResponseDto getPathInput() {

        List<Map<String, Object>> resultList = null;
        try {
            resultList = botStatementDao.getPathInput();
            System.out.println("<<< resultList: " + resultList);
            return new ResponseDto(1, "success", resultList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", resultList);
        }
    }

    @GetMapping("/executeBotStatement")
    public ResponseDto processFile(BotStatementSaveDto dto) {
        String projectCode = dto.getFilename();
        List<String> projectName = new ArrayList<>();
        int count = 0;
        List<String> moveFile = scanFilePath.listFilesForFolderToProcess(new File(fileStorageService.getFileInputLocation().toString()), projectCode);

        int fileListForScan = 0;
        for (String moveFileToProcess : moveFile) {
            File fileName = new File(moveFileToProcess);
            String getFileName = fileName.getName();
            projectName.add(getFileName);
            fileStorageService.moveFileToProcessDir(moveFileToProcess);
            fileListForScan++;
        }
        if (fileListForScan > 0) {
            List<String> fileList = scanFilePath.listFilesForFolderToProcess(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);
            for (String filename : fileList) {
                count = processJobBSTMT.jobRunner(filename);
            }
        }

        try {
            if(count > 0){
                return new ResponseDto(1, "success", null);
            }else {
                return new ResponseDto(0, "fail", null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", null);
        }
    }
}
