package com.newgfmisinterface.botstatement.service;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import com.newgfmisinterface.botstatement.validate.FileNameFormatValidate;

@Service
public class ScanFilePathService {
    private FileNameFormatValidate fileNameFormateEbiddingValidate = new FileNameFormatValidate();
    //botstatement
    public synchronized List<String> listFilesForFolder(File folder, String projectName) {

        ArrayList<String> result = new ArrayList<>();

        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, projectName);
            } else {

                String filename = fileEntry.getName();

                if (!fileNameFormateEbiddingValidate.isFileProjectCode(filename, projectName)) {
                    continue;
                }

                result.add(filename);
            }
        }

        return result;
    }
    public synchronized List<String> listFilesForFolderToProcess(File folder, String projectName) {

        ArrayList<String> result = new ArrayList<>();

        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolderToProcess(fileEntry, projectName);
            } else {

                String filename = fileEntry.getName();

                if (!fileNameFormateEbiddingValidate.isFileProjectToProcess(filename, projectName)) {
                    continue;
                }

                result.add(filename);
            }
        }

        return result;
    }
}
