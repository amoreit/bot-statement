package com.newgfmisinterface.botstatement.service;

import java.io.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.newgfmisinterface.botstatement.dao.SystemConfigDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.newgfmisinterface.botstatement.properties.FileStorageProperties;

@Service
public class FileStorageService {
    private static Logger LOGGER = LoggerFactory.getLogger(FileStorageService.class);
    private final Path fileStorageLocation;
    private final Path fileSuccessLocation;
    private final Path fileInputLocation;

    @Autowired
    private SystemConfigDao systemConfigDao;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties, SystemConfigDao systemConfigDao) {
        this.systemConfigDao = systemConfigDao;

        String inputPath = fileStorageProperties.getInputDir();
        try {
            inputPath = this.systemConfigDao.getInputFileConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String uploadPath = fileStorageProperties.getUploadDir();
        try {
            uploadPath = this.systemConfigDao.getUploadFileConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String successPath = fileStorageProperties.getSuccessDir();
        try {
            successPath = this.systemConfigDao.getSuccessFileConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.fileStorageLocation = Paths.get(uploadPath).toAbsolutePath().normalize();
        this.fileSuccessLocation = Paths.get(successPath).toAbsolutePath().normalize();
        this.fileInputLocation = Paths.get(inputPath).toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
            Files.createDirectories(this.fileSuccessLocation);
            Files.createDirectories(this.fileInputLocation);
        } catch (Exception ex) {
            System.out.println(ex);
            LOGGER.error("createDirectories " + ex);
        }
    }


    //BOT
    public void MoveFiletoSuccessDirDate(String filename) {
        String date = filename.substring(5, 13);
        File dirFileSuccess = new File(this.fileSuccessLocation.toString() + "/" + date);
        dirFileSuccess.mkdir();

        File fileupload = new File(this.fileStorageLocation.toString() + "/" + filename);
        String fileuploadfullpath = fileupload.getAbsolutePath();

        File fileSuccess = new File(this.fileSuccessLocation.toString() + "/" + date + "/" + filename);
        String fileSuccessfullpath = fileSuccess.getAbsolutePath();

        try {
            Files.move(Paths.get(fileuploadfullpath), Paths.get(fileSuccessfullpath), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("MoveFiletoSuccessDir " + e);
        }
    }
    //botstatement
    public void moveFileToProcessDir(String filename) {

        File fileupload = new File(this.fileInputLocation.toString() + "/" + filename);
        String fileuploadfullpath = fileupload.getAbsolutePath();

        File fileSuccess = new File(this.fileStorageLocation.toString() + "/" + filename);
        String fileSuccessfullpath = fileSuccess.getAbsolutePath();

        try {
            Files.move(Paths.get(fileuploadfullpath), Paths.get(fileSuccessfullpath), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("MoveFiletoSuccessDir " + e);
        }
    }

    //BOT
    public List<String> readFileByLine(final String filename) {

        ArrayList<String> result = new ArrayList<>();

        try {
            File fileDir = new File(filename);

            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF8"));

            String str;

            while ((str = in.readLine()) != null) {
                result.add(str);
            }

            in.close();
            return result;

        } catch (UnsupportedEncodingException e) {
            System.out.println(e.getMessage());
            LOGGER.error("UnsupportedEncodingException " + e);
            return result;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            LOGGER.error("IOException " + e);
            return result;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            LOGGER.error("Exception " + e);
            return result;
        }
    }

    public Path getFileStorageLocation() {
        return fileStorageLocation;
    }

    public Path getFileSuccessLocation() {
        return fileSuccessLocation;
    }

    public Path getFileInputLocation() {
        return fileInputLocation;
    }
}
